# User specification

## User logout
User has to be logged in

Tags: add new user
* Login
* Open userprofile
* Click on Logout

## Add new user to ANEXIA
User has to be logged in
Tags: create new user
* Login
* Click on Account within ANEXIA scope
* Click on Users&Teams
* Click on Add User
* Enter data within the modal
* Search for User 

## Delete user 
User has to be logged in
* Login
* Click on Account within ANEXIA scope
* Click on Users&Teams
* Search for User
