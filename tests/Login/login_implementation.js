const {Builder, By, Capabilities, Key, until, WebDriver, WebElement } = require('selenium-webdriver')
var CustomWebDriver = require('../driver') 
const fs = require('fs');

let driver_ = new CustomWebDriver();
let driver

beforeScenario(function () {
    driver = driver_.initializeDriver();
  });
  
  afterScenario(function(){
    //driver.quit();
  })


step("Goto anexia internal-engine page", async function () {

    //ToDo-Add cookie to CookieData for further spec without login steps
    fs.writeFile('cookieData.txt');
    driver.get('https://integration.ps.anexia-it.com/login');
});

step("Enter valid username", async function () {
    await driver.findElement(By.xpath('//*[@id="login-username"]')).sendKeys('JaneDoe@anexia-it.com');
});

step("Click on Continue", async function () {
    await driver.findElement(By.xpath('//*[@id="login-submit"]')).click();

});

step("Enter valid password", async function () {
    await driver.findElement(By.xpath('//*[@id="login-password"]')).sendKeys('testUser07@2020');
});

step("Enter invalid username", async function () {
    await driver.findElement(By.xpath('//*[@id="login-username"]')).sendKeys('JaneDoe@');
});

step("Enter invalid password", async function () {
    await driver.findElement(By.xpath('//*[@id="login-password"]')).sendKeys('testUser07');
});

step("Verify text of errorMsg", async function () {

    driver.sleep(4000);

    await driver.findElement(By.xpath('/html/body/div[6]/div'))
        .getText()
        .then(function (errorMsg) {
            if (errorMsg === 'Username and/or password invalid.')
                console.log("passing test");
            else {
                //ToDo: let testCase failing
                console.log("failing test");
            }
        })
        .catch(function (err) {
            console.log("Error ", err, " occurred!");
        });

});

step("Click on Login", async function () {
    await (await driver.findElement(By.xpath('//*[@id="login-submit"]'))).click();
});

step("Activate checkbox to stay signed in", async function () {
    await driver.findElement(By.xpath('/html/body/div[1]/div[1]/div/div/form/div[5]/div/div/div[1]/label/div')).click();
});

step("Verify if user is logged in after browser was closed", async function () {

    driver.get("https://integration.ps.anexia-it.com");
    await driver.findElement(By.xpath('/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/ul/li[4]/a/span'));
});

step("Verify if login was successful", async function () {

    await driver.findElement(By.xpath('/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/ul/li[4]/a/span'))
        
        .getText()
        .then(function (username) {

            console.log("username is: " + username);
            if (username === 'Jane Doe') {

                console.log("Login was successful");

            }
            else {
                //ToDo: let testCase failing
                console.log("Login was not successful");
            }
        })
});
