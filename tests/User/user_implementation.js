const {Builder, By, Capabilities, Key, until, WebDriver, WebElement } = require('selenium-webdriver')
var CustomWebDriver = require('../driver') 
const fs = require('fs');

let driver_ = new CustomWebDriver();
let driver

beforeScenario(function () {
    driver = driver_.initializeDriver();
  });
  
  afterScenario(function(){
    //driver.quit();
  })

  step('Login', async () => {
  
    driver.get('https://integration.ps.anexia-it.com/login')
  await driver
    .findElement(By.xpath('//*[@id="login-username"]'))
    .sendKeys('JaneDoe@anexia-it.com')
  await driver.findElement(By.xpath('//*[@id="login-submit"]')).click()
  await driver.findElement(By.xpath('//*[@id="login-password"]')).sendKeys('testUser07@2020')
  await driver.findElement(By.xpath('//*[@id="login-submit"]')).click()
  
  await driver.findElement(
      By.xpath('/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/ul/li[4]/a/span')
    )
    //.css('input#login-username'))
    //
    .getText()
    .then(function (username) {
      console.log('username is: ' + username)
      if (username === 'Jane Doe') {
        console.log('Login was successful')
      } else {
        //ToDo: let testCase failing
        console.log('Login was not successful')
      }
    })
})
step('Open userprofile', async () => {
  driver.wait(
    until.elementLocated(
      By.xpath('/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/ul/li[4]/a/span')
    ),
    5000
  )

  await driver
    .findElement(
      By.xpath('/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/ul/li[4]/a/span')
    )
    .click()
})

step('Click on Logout', async () => {
  driver.wait(function () {
    driver
      .findElement(By.xpath('//*[@id="anx-user-menu"]/ul/li[4]/ul/li[10]/a'))
      .click()
  })
})
step("Click on Account within ANEXIA scope", async function () {
  driver.wait(
    until.elementLocated(
      By.xpath("//*[@id='anx-modules']/div[2]/a")
    ),
    5000
  )

    driver.findElement(By.xpath("//*[@id='anx-modules']/div[2]/a")).click();
    driver.findElement(By.xpath("//*[@id='anx-modules']/div[2]/div/ul/li[1]/a")).click();

});

step("Click on Users&Teams", async function () {

   // driver.findElement(By.xpath("//*[@id='anx-page-navigation']/ul/li[4]/a")).click();
     

driver.wait(until.elementLocated(By.xpath("//*[@id='anx-page-navigation']/ul/li[4]/a")), 5000);
driver.findElement(By.xpath("//*[@id='anx-page-navigation']/ul/li[4]/a")).click().then(function() {
    console.log("Users&Teams was clicked");
  
});

});

step("Click on Add User", async function () {

    
    var add_btn = driver
    .findElement(By.xpath("//*[@id='dt-users_wrapper']/div/div[2]/div[2]/a[2]"));

   driver.wait(until.elementLocated(add_btn,100));

    

    if (add_btn != null) {
       console.log("Element is present");
        add_btn.click();
    } else {
        console.log("Element is absent");
    }

});

step("Enter data within the modal", async function () {
   
   driver.wait(until.elementLocated(By.xpath("//*[@id='unimodal']/div/div/div[2]/div[2]")));

    //--------------------------------USER INFORMATION--------------------------------
    let email_r = driver.findElement(By.xpath("//*[@id='formEmail']"));
    email_r.sendKeys("AcceptanceTesting@anexia-it.com");
    let firstname = driver.findElement(By.xpath("//*[@id='formFirstName']"));
    firstname.sendKeys("Acceptance");
    let lastname_r = driver.findElement(By.xpath("//*[@id='formLastName']"));
    lastname_r.sendKeys("Testing");
    //Select drpLanguage = new Select(driver.findElement(By.xpath("//*[@id='s2id_formLanguage']/a")));
    //drpLanguage.selectByIndex(1);
    //Select drpPermissionGroups = new Select(driver.findElement(By.xpath("//*[@id='s2id_formPermissionGroups']/ul")));
    //drpPermissionGroups.selectByVisibleText("D17 - Platform Solutions");
    // select teams

    driver.findElement(By.xpath("//*[@id='unimodal']/div/div/div[3]/button[2]")).click();
    //--------------------------------USER INFORMATION--------------------------------

    //--------------------------------ADDITIONAL INFORMATION--------------------------------
    let position = driver.findElement(By.xpath("//*[@id='formPosition']"));
    position.sendKeys("QA-Engineer");
    let address = driver.findElement(By.xpath("//*[@id='formAddress']"));
    address.clear();
    address.sendKeys("Feldkirchnerstraße 144");
    let zip = driver.findElement(By.xpath("//*[@id='formZip']"));
    let city = driver.findElement(By.xpath("//*[@id='formCity']"));

    let phone = driver.findElement(By.xpath("//*[@id='formPhone']"));
    phone.sendKeys("+43 4243-37798");
    let mobilePhone = driver.findElement(By.xpath("//*[@id='formMobilePhone']"));
    mobilePhone.sendKeys("+43 650/9250018");
    driver.findElement(By.xpath("//*[@id='unimodal']/div/div/div[3]/button[2]")).click();
    //--------------------------------ADDITIONAL INFORMATION--------------------------------
    
    
     //--------------------------------PRIVATE INFORMATION--------------------------------
     driver.findElement(By.xpath("//*[@id='unimodal']/div/div/div[3]/button[2]")).click();
    //--------------------------------PRIVATE INFORMATION--------------------------------

});

step("Search for User", async function () {
   
   
    driver.wait(until.elementIsVisible(By.xpath("//*[@id='unimodal']/div/div/div[2]/div[2]")));


    driver.wait(until.elementIsVisible(By.xpath("//*[@id='dt-users_filter']/span/i")));

    driver.findElement(By.xpath("//*[@id='dt-users_filter']/span/i")).click();
    driver.findElement(By.xpath("//*[@id='dt-users_filter']/div/input")).sendKeys("Acptance");
 
 });