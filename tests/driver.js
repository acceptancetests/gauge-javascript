
const { ServiceBuilder } = require('selenium-webdriver/chrome')
const firefox = require('selenium-webdriver/firefox')
const webdriver = require('selenium-webdriver')

let browser = process.env.browser

let chromeDriverPath = process.env.chromedriver
let firefoxDriverPath = process.env.firefoxdriver
let firefoxOptions = new firefox.Options().setBinary(firefox.Channel.NIGHTLY)
let driver
let _instance = null

class CustomWebDriver {
		
     initializeDriver() {

		console.log(`Browser: ${browser}`)
        if (browser === 'chrome') {
            //const serviceBuilder = new ServiceBuilder(chromeDriverPath)

            driver = new webdriver.Builder()
                .withCapabilities(webdriver.Capabilities.chrome())
                .build()

            driver.manage().window().maximize()
            // .forBrowser('chrome')
            // .setChromeService(serviceBuilder)
            // .build()
        }
        if (browser === 'firefox') {
            driver = new webdriver.Builder()
                .forBrowser('firefox')
                .setFirefoxOptions(firefoxOptions)
                .build()
                }
                
				if (_instance === null) {
					_instance = driver;
					return _instance;
				}
        return _instance;
    }
}

module.exports = CustomWebDriver